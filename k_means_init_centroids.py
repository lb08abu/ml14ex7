
import numpy as np


def k_means_init_centroids(X, num_centroids):
    randomized_X = np.random.permutation(X)
    return randomized_X[0:num_centroids, :]