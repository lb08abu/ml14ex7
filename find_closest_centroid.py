
import numpy as np


def find_nearest_centroid(X, centroids):
    """
    :param X: Numpy array of feature vectors
    :param centroids: Numpy array of centroids
    :return: Indexes of nearest centroids for X
    """

    K = centroids.shape[0]
    I, J = X.shape

    # set up the idx array
    idx = np.zeros((I, 1))

    for i in range(I):

        # calculate the distance from the 0th centroid as a
        # starting point
        # we use the magnitude of the vector difference to
        # get the distance, np.linalg.norm()
        dist = np.linalg.norm(X[i, :] - centroids[0, :])

        # we can start from index 1 of centroids as we've
        # already calculated for 0
        for k in range(1, K):

            # if the distance to to centroid[k] is less than
            # the current distance, set the new distance and
            # assign the index to that centroid
            if np.linalg.norm(X[i, :] - centroids[k, :]) < dist:
                dist = np.linalg.norm(X[i, :] - centroids[k, :])
                idx[i] = k

    return idx