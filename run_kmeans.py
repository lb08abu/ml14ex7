#!/use/bin/env python3

import numpy as np
from find_closest_centroid import find_nearest_centroid
from compute_centroids import compute_centroids


def run_kmeans(X, init_centroids, max_iters=10, plot_progress=False):
    """
    Hello :)

    :param X:
    :param initial_centroids:
    :param max_iters:
    :param plot_progress:
    :return:
    """

    m, n = X.shape
    K = init_centroids.shape[0]
    centroids = init_centroids
    p_centroids = centroids.copy()

    for i in range(max_iters):
        print('K-means iter: {}/{}'.format(i+1, max_iters))

        # find the nearest centroids
        idx = find_nearest_centroid(X, centroids)

        # determine new centroid locations
        centroids = compute_centroids(X, idx, centroids)

        p_centroids = np.vstack((p_centroids, centroids))

    return idx, p_centroids