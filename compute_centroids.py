import numpy as np


def compute_centroids(X, idx, centroids):
    """
    Computes new centroid locations.
    """

    K = centroids.shape[0]
    I, J = X.shape

    for k in range(K):

        # create list of relevant X examples
        a = np.array([X[i, :] for i in range(I) if idx[i, :] == k])

        # the new centroid location is the average of all of these
        # elements along the 0th axis (downwards)
        centroids[k] = np.average(a, axis=0)

    return centroids
